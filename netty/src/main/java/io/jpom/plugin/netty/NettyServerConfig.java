package io.jpom.plugin.netty;

import cn.hutool.core.thread.ThreadUtil;
import cn.jiangzeyin.common.DefaultSystemLog;
import io.jpom.plugin.PluginFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextClosedEvent;

import javax.annotation.Resource;

/**
 * netty 服务检测
 *
 * @author bwcx_jzy
 * @date 2019/8/12
 */
@Configuration
public class NettyServerConfig implements ApplicationListener {

    /**
     * 程序端口
     */
    @Value("${netty.port:8888}")
    private int port;

    private NettyThread nettyThread;

    @Resource
    private NettyFeatureCallback nettyFeatureCallback;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ApplicationReadyEvent) {
            if (port <= 0) {
                DefaultSystemLog.getLog().info("端口配置错误：" + port);
                return;
            }
            nettyThread = new NettyThread(port);
            ThreadUtil.execute(nettyThread);
            // 添加方法回调
            PluginFactory.addFeatureCallback(nettyFeatureCallback);
            return;
        }
        if (event instanceof ContextClosedEvent) {
            if (nettyThread != null) {
                DefaultSystemLog.getLog().info("关闭netty");
                try {
                    nettyThread.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
