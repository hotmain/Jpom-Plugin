import io.jpom.util.HttpUtils;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author bwcx_jzy
 * @date 2019/11/18
 */
public class TestHttp {

    @Test
    public void test() {
        Map<String, String> header = new HashMap<>();
        Map<String, String> form = new HashMap<>();
        String s = "http://127.0.0.1:9001/iptv_jpom//node/manage/saveProject";

        header.put("JPOM-USER-TOKEN", "5610b7db99f7216e4ed3543f2a56eb95");
//        //
        form.put("nodeId", "localhost");
//        //
        form.put("name", ":");
        form.put("group", "ss");
        form.put("id", "ss");
        form.put("runMode", "File");
        form.put("whitelistDirectory", "/test/");
        form.put("lib", "dfgdsfg");
        //
        form.put("mainClass", "");
        form.put("jvm", "");
        form.put("args", "");
        form.put("token", "");
        String post = HttpUtils.post(s, form, header, (int) TimeUnit.MINUTES.toMillis(1), (int) TimeUnit.MINUTES.toMillis(1), "utf-8");
        System.out.println(post);
    }
}
